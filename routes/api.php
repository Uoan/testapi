<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductPricesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('prices')->group(function () {
    Route::get('/', [ProductPricesController::class, 'getPrices']);
    Route::post('/{product}', [ProductPricesController::class, 'updatePrices']);
});
