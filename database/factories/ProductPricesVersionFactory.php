<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductPricesVersion>
 */
class ProductPricesVersionFactory extends Factory
{

    public function definition(): array
    {

        return [
            'product_guid' => !Product::exists() ? Product::factory()->create()->guid : Product::inRandomOrder()->first()->guid,
            'prices' => $this->getPricesArray(),
            'version' => microtime(true)
        ];
    }

    private function getPricesArray(): array
    {
        $pricesCount = $this->faker->numberBetween(1, 99);

        for ($i = 0; $i < $pricesCount; $i++) {
            $prices[] = [
                'guid' => Str::uuid(50),
                'price' => $this->faker->numberBetween(0, 100000)
            ];
        }

        return $prices;
    }
}
