<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices_versions', function (Blueprint $table) {
            $table->id();
            $table->string('product_guid', 100);
            $table->json('prices');
            $table->timestamps();
            $table->string('version', 100);


            $table->unique(['product_guid','version']);

            $table->foreign('product_guid')->references('guid')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices_versions');
    }
};
