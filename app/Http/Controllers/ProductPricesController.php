<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\ProductPricesVersion;
use Illuminate\Database\Query\JoinClause;
use App\Http\Requests\UpdatePricesRequest;

class ProductPricesController extends Controller
{
    // по хорошему в этом контроллере должен быть репозитории и ресурсы для цен

    public function getPrices(): JsonResponse
    {

        $latestPrices = DB::table('product_prices_versions')
            ->select(['product_guid', DB::raw('MAX(created_at) as last_prices')])
            ->groupBy('product_guid');

        $prices = ProductPricesVersion::select([
            DB::raw('product_prices_versions.product_guid as product'),
            DB::raw('product_prices_versions.prices as prices')
        ])
            ->joinSub($latestPrices, 'product_prices_versions_sub', function (JoinClause $join) {
                $join->on('product_prices_versions_sub.product_guid', '=', 'product_prices_versions.product_guid');
            });

        return response()->json($prices->paginate());
    }

    public function updatePrices(Product $product, UpdatePricesRequest $request): JsonResponse
    {
        $data = $request->validated();

        $createStatus = ProductPricesVersion::create([
            'product_guid' => $product->guid,
            'prices' => $data['prices'],
            'version' => microtime(true)
        ]);

        return response()->json(['message' =>  $createStatus ? 'success' : 'failed']);
    }
}
