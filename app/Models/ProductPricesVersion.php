<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductPricesVersion extends Model
{
    use HasFactory;

    protected $casts = [
        'prices' => 'array'
    ];

    protected $guarded = [];
}
