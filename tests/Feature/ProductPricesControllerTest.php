<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Product;
use App\Models\ProductPricesVersion;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductPricesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testCreatePrices()
    {

        $guid = Product::factory()->create()->guid->toString();

        $prices = ProductPricesVersion::factory()->make()->prices;

        $this->postJson("api/prices/$guid", ['prices' => $prices])
            ->assertOk();

        $this->assertDatabaseHas('product_prices_versions', ['product_guid' =>  $guid]);
    }

    public function testCreateMultiple()
    {

        $prices = ProductPricesVersion::factory()->count(200)->make();

        foreach ($prices as $price) {
            $guid = Product::factory()->create()->guid->toString();
            $this->postJson("api/prices/$guid", ['prices' => $price->prices])
                ->assertOk();

            $this->assertDatabaseHas('product_prices_versions', ['product_guid' =>  $guid]);
        }
    }

    public function testGetPrices()
    {

        $pricesVersion = ProductPricesVersion::factory()->count(10)->create();

        $firstPrice = $pricesVersion->first();

        $this->get('api/prices')
            ->assertOk()
            ->assertJsonFragment(['product' =>   $firstPrice->product_guid, 'prices' =>   $firstPrice->prices]);
    }
}
